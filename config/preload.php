<?php

if (file_exists(dirname(__DIR__).'/var/cache/prod/App_KernelProdContainer.preload.php')) {
    require dirname(__DIR__).'/var/cache/prod/App_KernelProdContainer.preload.php';
}


define('ROOT_DIR', implode('/', $dir_array) . '/');
define('FILE_DIR', ROOT_DIR . 'public/uploads/files/');
define('FILE_URL', SITE_URL . 'uploads/files/');
define('FILE_PRIV_DIR', ROOT_DIR . 'uploads/files/');
$logsDirFile = ROOT_DIR . 'var/log/' . str_replace('.', '_', DB_SITE_DOMAIN);
if(!is_dir($logsDirFile)){
    @mkdir($logsDirFile, 0777);
}
define('DIR_LOGS', $logsDirFile . '/');