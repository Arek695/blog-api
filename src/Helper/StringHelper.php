<?php

namespace App\Helper;

class StringHelper {

    /**
     * Przydatne
     * @param string $var
     * @return string
     */
    static public function filterStr($var): string
    {
        return trim(strip_tags(filter_var(
            str_replace(['<?php', '<?=', '?>', '<script>', '</script>'], '', $var)
        )));
    }

    static public function filterFormatedStr($var) {
        $remove = [
            '<?php', '<?=', '?>', '<?', '<script>', '</script>', '<div></div>', '<p></p>', '<div><br></div>', '<p><br><p>',
            '<div><br/></div>', '<p><br/></p>'
        ];
        $resultRemove = trim(str_replace($remove, '', $var));
        $result = self::removeMultipleChar($resultRemove);
        self::addSlashes($result);
//		dd(compact('var', 'resultRemove', 'result'));
        return $result;
    }

    static private function removeMultipleChar(string $textToClear, ?array $charsToCheck = []) {
        $myBeSingleChars = ['?', '=', '-', ' ', '+', '!', '|', '\\'];
        if ($charsToCheck) {
            $myBeSingleChars = array_merge($myBeSingleChars, $charsToCheck);
        }
        foreach ($charsToCheck as $ch) {
            while (strpos($textToClear, $ch . $ch) !== false) {
                $textToClear = str_replace($ch . $ch, $ch, $textToClear);
            }
        }

        return $textToClear;
    }

    static public function filterEmail($var) {
        return trim(filter_var(
            str_replace(['<?php', '<?=', '?>', '<script>', '</script>', '>', '<', '[', ']', '{', '}', '(', ')'], '', $var),
            FILTER_SANITIZE_EMAIL
        ));
    }

    static public function filterUrl($var) {
        return trim(filter_var(
            str_replace(['<?php', '<?=', '?>', '<script>', '</script>', '>', '<', '[', ']', '{', '}', '(', ')'], '', $var),
            FILTER_SANITIZE_URL
        ));
    }

    static public function filterInt($var) {
        return trim(strip_tags(filter_var(
            str_replace(['<?php', '<?=', '?>', '<script>', '</script>'], '', $var),
            FILTER_SANITIZE_NUMBER_INT
        )));
    }

    static public function filterFloat($var) {
        return trim(strip_tags(filter_var(
            str_replace(['<?php', '<?=', '?>', '<script>', '</script>'], '', $var),
            FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION
        )));
    }

    static public function filterBool($var) {
        return trim(strip_tags(filter_var(
            str_replace(['<?php', '<?=', '?>', '<script>', '</script>'], '', $var),
            FILTER_VALIDATE_BOOLEAN
        )));
    }

    /**
     * Code for users (default $length=32) and other
     *
     * @param int $length
     * @return string
     */
    static public function uniqueCode(int $length = 32, bool $strLower = false) {
        if (!$length || $length < 0) {
            return '';
        }
        $code = '';
        if ($strLower) {
            $chars = array_merge(range(0, 9), range('a', 'z'));
        } else {
            $chars = array_merge(range(0, 9), range('a', 'z'), range('A', 'Z'));
        }
        $max = count($chars) - 1;
        for ($i = 0; $i < $length; $i++) {
            $code .= $chars[mt_rand(0, $max)];
        }

        return $code;
    }

    /**
     * Rozwiązanie problemu z nazwami plików, bo pomysłowość ludzka nie zna granic
     *
     * @param string $filename
     * @param int $length
     * @param int $addUniqueCodeLength
     * @return string
     */
    static public function safeFileName(string $filename, int $length = 50, int $addUniqueCodeLength = 0) {
        if (!$filename || !self::clearString($filename)) {
            return null;
        }
        $safe = strtolower(self::clearString($filename));
        $s = explode('.', (string) $safe);
        $ext = array_pop($s);
        $name = implode('.', $s);
        if (!$ext || !$name) {
            return $safe;
        }
        $dot_ext = '.' . $ext;
        $newLength = $addUniqueCodeLength && $addUniqueCodeLength > 0 ?
            strlen($safe) + $addUniqueCodeLength + 1 : strlen($safe);
        if ($newLength > $length) {
            $name = substr($name, 0, $newLength - ($newLength - $length));
        }
        if ($addUniqueCodeLength) {
            $name .= '-' . self::uniqueCode($addUniqueCodeLength, 1);
        }

        return trim($name, '-') . $dot_ext;
    }


    static public function clearName(string $name, bool $withSpace = true) {
        $str = self::filterStr($name);
        $change = $withSpace ? ['/', '_', '|', '+'] : ['/', '_', '|', '+', ' '];
        return trim(trim(trim(mb_ereg_replace("/[^a-zA-Z0-9-.,\s]/", '', str_replace($change, '-', trim($str))), '-'), '.'));
    }

    static public function clearChars(string $string) {
        $bad = ['À', 'Á', 'Â', 'Ã', 'Ä', 'Å', 'Æ', 'Ç', 'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Î', 'Ï', 'Ð', 'Ñ', 'Ò', 'Ó', 'Ô', 'Õ', 'Ö', 'Ø', 'Ù', 'Ú', 'Û', 'Ü', 'Ý', 'ß', 'à', 'á', 'â', 'ã', 'ä', 'å', 'æ', 'ç', 'è', 'é', 'ê', 'ë', 'ì', 'í', 'î', 'ï', 'ñ', 'ò', 'ó', 'ô', 'õ', 'ö', 'ø', 'ù', 'ú', 'û', 'ü', 'ý', 'ÿ', 'Ā', 'ā', 'Ă', 'ă', 'Ą', 'ą', 'Ć', 'ć', 'Ĉ', 'ĉ', 'Ċ', 'ċ', 'Č', 'č', 'Ď', 'ď', 'Đ', 'đ', 'Ē', 'ē', 'Ĕ', 'ĕ', 'Ė', 'ė', 'Ę', 'ę', 'Ě', 'ě', 'Ĝ', 'ĝ', 'Ğ', 'ğ', 'Ġ', 'ġ', 'Ģ', 'ģ', 'Ĥ', 'ĥ', 'Ħ', 'ħ', 'Ĩ', 'ĩ', 'Ī', 'ī', 'Ĭ', 'ĭ', 'Į', 'į', 'İ', 'ı', 'Ĳ', 'ĳ', 'Ĵ', 'ĵ', 'Ķ', 'ķ', 'Ĺ', 'ĺ', 'Ļ', 'ļ', 'Ľ', 'ľ', 'Ŀ', 'ŀ', 'Ł', 'ł', 'Ń', 'ń', 'Ņ', 'ņ', 'Ň', 'ň', 'ŉ', 'Ō', 'ō', 'Ŏ', 'ŏ', 'Ő', 'ő', 'Œ', 'œ', 'Ŕ', 'ŕ', 'Ŗ', 'ŗ', 'Ř', 'ř', 'Ś', 'ś', 'Ŝ', 'ŝ', 'Ş', 'ş', 'Š', 'š', 'Ţ', 'ţ', 'Ť', 'ť', 'Ŧ', 'ŧ', 'Ũ', 'ũ', 'Ū', 'ū', 'Ŭ', 'ŭ', 'Ů', 'ů', 'Ű', 'ű', 'Ų', 'ų', 'Ŵ', 'ŵ', 'Ŷ', 'ŷ', 'Ÿ', 'Ź', 'ź', 'Ż', 'ż', 'Ž', 'ž', 'ſ', 'ƒ', 'Ơ', 'ơ', 'Ư', 'ư', 'Ǎ', 'ǎ', 'Ǐ', 'ǐ', 'Ǒ', 'ǒ', 'Ǔ', 'ǔ', 'Ǖ', 'ǖ', 'Ǘ', 'ǘ', 'Ǚ', 'ǚ', 'Ǜ', 'ǜ', 'Ǻ', 'ǻ', 'Ǽ', 'ǽ', 'Ǿ', 'ǿ'];
        $good = ['A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o'];

        return trim(str_replace($bad, $good, self::filterStr($string)));
    }

    static public function clearSlug(string $string, ?int $maxLength = 100) {
        $ext = strpos($string, 'http') !== false ? true : false;
        if ($ext) {
            $changeChars = [' ', '\\', '<', '>', '{', '}', '[', ']', '(', ')', '*', '^', '%', '$', '!', '"', '\''];
            $string = preg_replace("/[^a-zA-Z0-9&?=+-_|\/@#,.:]/", '', str_replace($changeChars, '-', self::clearChars(self::filterFormatedStr($string))));
        } else {
            $changeChars = [' ', '\\', '<', '>', '{', '}', '[', ']', '(', ')', '*', '^', '%', '$', '!', '"', '\'', ':'];
            $string = preg_replace("/[^a-zA-Z0-9&?=+-_|\/@#,.]/", '', str_replace($changeChars, '-', self::clearChars(self::filterStr($string))));
        }
        while (strpos($string, '--') !== false) {
            $string = str_replace('--', '-', $string);
        }

        $result = strtolower(trim($string, '&?=+-_|\/@#,.]/'));

        return strlen($result) > $maxLength ? substr($result, 0, $maxLength - 1) : $result;
    }

    /**
     * Wyczyszczenie stringa z ogonków + innych znaków, spoza liter ANSI, cyfr oraz "-" i kropki
     *
     * @param string $string
     * @param bool $onlyMinusAndDot
     * @param bool $smallLetters
     * @return string
     */
    static public function clearString(string $string, bool $onlyMinusAndDot = true, bool $smallLetters = true) {
        $str = self::clearChars($string);
        if ($onlyMinusAndDot) {
            $changeChars = ['/', '_', '|', '+', ' '];
            $str = trim(trim(preg_replace("/[^a-zA-Z0-9-.]/", '', str_replace($changeChars, '-', $str)), '-'), '.');
        }

        if ($smallLetters) {
            $str = strtolower($str);
        }
        return $str;
    }

    /**
     * Dodaje znak \ przed ' oraz " w array'u lub stringu
     * Można wykorzystać przed wysłaniem danych do repozytorium.
     *
     * @param array $data
     */
    static public function addSlashes(&$data) {
        if (\is_array($data)) {
            self::arrayAddSlashes($data);
        } elseif (is_string($data)) {
            $data = \addslashes($data);
        }
    }

    static private function arrayAddSlashes(array &$data) {
        foreach ($data as &$item) {
            self::addSlashes($item);
        }
    }

    /**
     * Odwrotność addSlashes
     *
     * @param array|string $data
     */
    static public function unSlashes(&$data) {
        if (is_array($data)) {
            self::arrayUnSlashes($data);
        } elseif (is_string($data)) {
            $data = stripcslashes($data);
        }
    }

    static private function arrayUnSlashes(array &$data) {
        foreach ($data as &$item) {
            self::unSlashes($item);
        }
    }

    /**
     * Zwraca wartość numeryczną ze stringu
     *
     * @param string|int $number
     */
    static public function numeric($number) {
        return !is_numeric($number) && is_string($number) ?
            str_replace(' ', '', str_replace(',', '.', $number)) :
            $number;
    }

    /*
     * name_value to nameValue
     */
    static public function snakeToCamelCase(string $string, string $wordBefore = ''): string {
        $str = strlen(trim($wordBefore)) ? trim($wordBefore) . '_' . trim($string) : trim($string);
        $arr = explode('_', $str);
        $camel = '';
        foreach ($arr as $key => $value) {
            $camel .= $key ? ucfirst($value) : $value;
        }

        return $camel;
    }

    /*
     * nameValue to name_value
     */

    static public function camelToSnakeCase(string $string, string $before = ''): string {
        $str = strlen(trim($before)) ? strtolower($before) . ucfirst(trim($string)) : trim($string);
        return ltrim(strtolower(preg_replace('/[A-Z]([A-Z](?![a-z]))*/', '_$0', $str)), '_');
    }

    static public function textNlToHtml(?string $text): ?string {
        $safeText = $text ? self::filterStr($text) : '';
        $safeArr = [];
        if (strlen($safeText)) {
            $safeArr = explode(PHP_EOL, $safeText);
            foreach ($safeArr as &$line) {
                $l = trim($line);
                if (!empty($l) && ($l[0] <> "<")) {
                    $line = "<p>$l</p>" . PHP_EOL;
                }
            }
        }

        return count($safeArr) ? implode("", $safeArr) : null;
    }

}
