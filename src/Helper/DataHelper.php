<?php

namespace App\Helper;

use Doctrine\ORM\Tools\Pagination\Paginator;

class DataHelper {

    // kiedy rezutlat jest arrayem (pytanie zawiera select) $outputWalkers musi być false.
    // Nie stosować przy uzyciu paginate select, jeśli używasz w pyteniu having oraz ustaw wtedy true dla $outputWalkers,
    static public function paginate($query, int $page = 1, int $limit = 100, $fetchJoinCollection = true, $outputWalkers = false) {
        $paginator = new Paginator($query, $fetchJoinCollection);
        $paginator->setUseOutputWalkers($outputWalkers);
        $paginator->getQuery()->setFirstResult($limit * ($page - 1))->setMaxResults($limit);
        $all = $paginator->count();
        $data = $paginator->getIterator();
        $rows = $data->count();
        $pages = (int) ceil($all/$limit);
        return compact('data', 'rows', 'all', 'page', 'pages', 'limit');
    }

    static public function paginateAll($query, int $page = 1, int $limit = 100, $fetchJoinCollection = true, $outputWalkers = false) {
        $paginator = new Paginator($query, $fetchJoinCollection);
        $paginator->setUseOutputWalkers($outputWalkers);
//        $paginator->getQuery()->setFirstResult(1)->setMaxResults($limit);
        $all = $paginator->count();
        $data = $paginator->getIterator();
        $rows = $data->count();
        $pages = (int) ceil($all/$limit);
        return compact('data', 'rows', 'all', 'page', 'pages', 'limit');
    }

}
