<?php


namespace App\Controller\Home;

use App\Controller\DefaultController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;


class HomeController extends DefaultController
{

    /**
     * Strona głowna
     *
     * @Route("/home", name="Home", schemes={"http"})
     */
    public function index(){
        return new Response(
            '<html><body>text</body></html>'
        );
    }


}