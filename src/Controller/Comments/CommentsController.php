<?php


namespace App\Controller\Comments;


use App\Controller\DefaultController;
use App\Repository\CommentsRepository;
use App\Service\CommentsService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class CommentsController extends DefaultController
{
    /**
     * Lista komentarzy
     *
     * @Route("/comments/{post_id}", name="commentsList", methods={"GET"}, schemes={"http"})
     */
    public function commentsList(CommentsRepository $commentsR, CommentsService $commentsS, int $post_id): JsonResponse
    {
        $data = $commentsS->getCommentsFor($post_id);
        return new JsonResponse($this->setResponseData($data));
    }

    /**
     * Dodanie komentarza dla danego postu
     *
     * @Route("/comments/{post_id}/add", name="addComment", methods={"POST"}, schemes={"http"})
     */
    public function addComment(Request $request, CommentsRepository $commentsR, CommentsService $commentsS, int $post_id): JsonResponse
    {
        $query = $request->request->all();
        $data = $commentsS->addCommentFor($post_id, $query);
        return new JsonResponse($this->setResponseData($data));
    }

    /**
     * Usuwanie komentarza dla danego postu
     *
     * @Route("/comments/{post_id}/remove", name="removeComment", methods={"GET"}, schemes={"http"})
     */
    public function removeComment(CommentsRepository $commentsR, CommentsService $commentsS, int $post_id): JsonResponse
    {
        $data = $commentsS->removeCommentFor($post_id);
        return new JsonResponse($this->setResponseData($data));
    }


}