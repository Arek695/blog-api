<?php


namespace App\Controller\Posts;


use App\Controller\DefaultController;
use App\Repository\PostsRepository;
use App\Service\PostsService;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class PostsController extends DefaultController
{

    /**
     * Lista postów
     *
     * @Route("/posts/all/{page}", defaults={"page"=1}, name="index", methods={"GET"}, schemes={"http"})
     */
    public function index(PostsRepository $postsR, PostsService $postsS, int $page){
        $data = $postsS->postsContent($page);
        return new JsonResponse($this->setResponseData($data));
    }

    /**
     * Dodawanie postów
     *
     * @Route("/posts/add", name="add_post",  methods={"POST"}, schemes={"http"})
     */
    public function addPost(Request $request, PostsRepository $postsR, PostsService $postsS){
        $query = $request->query->all();
        $data = $postsS->addPost($query);
        return new JsonResponse($data);
    }

    /**
     * Pojedynczy post
     *
     * @Route("/posts/{post_id<[1-9]\d*>}",  methods={"GET"}, name="post", schemes={"http"})
     */
    public function singlePost(PostsRepository $postsR, PostsService $postsS, int $post_id){
        $data = $postsS->getPost($post_id);
        return new JsonResponse($this->setResponseData($data));
    }

    /**
     * @Route("/posts/edit", methods={"POST"}, name="edit_post")
     */
    public function editPost(Request $request, PostsRepository $postsR, PostsService $postsS) {
        $query = $request->query->all();
        $data = $postsS->editPost($query);
        return new JsonResponse($this->setResponseData($data));
    }

    /**
     * @Route("/posts/{post_id<[1-9]\d*>}/remove", methods={"GET"}, name="remove_post")
     */
    public function removePost(int $post_id, PostsRepository $postsR, PostsService $postsS) {
        $data = $postsS->removePost($post_id);
        return new JsonResponse($this->setResponseData($data));
    }

}