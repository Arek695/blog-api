<?php


namespace App\Controller;


use App\Entity\User;
use App\Helper\EmailHelper;
use App\Helper\StringHelper;
use App\Repository\UserRepository;
use App\Repository\UserTypeRepository;
use App\Security\LoginAuthenticator;
use App\Service\DbService;
use App\Service\UserService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Guard\GuardAuthenticatorHandler;

class RegisterController
{

    /**
     * Rejestracja
     *
     * @Route("/register", name="registerMail", methods={"POST"})
     */
    public function registerMail(
        UserService $userService,
        DbService $dbS,
        Request $request,
        UserPasswordEncoderInterface $passwordEncoder,
        GuardAuthenticatorHandler $guardHandler,
        LoginAuthenticator $authenticator,
        UserTypeRepository $userTypeR,
        UserRepository $userR,
    )
    {
        if ($request->isMethod('post')) {
            $username = StringHelper::clearName($request->request->get("username"));
            if (!strlen($username)) {
                $message = "Pole imię i nazwisko jest nieprawidłowe";
                return [$message];
            }

            $email = trim(strtolower($request->request->get("email")));
            if (!EmailHelper::validateEmail($email)) {
                $message = "Email $email jest błędny";
                return [$message];
            } elseif ($userR->loadUserByUsername($email)) {
                $message = "Email $email jest już zarejestrowany.";
                return [$message];
            }
            $pass = $request->request->get("password");

//            if ($pass !== $request->request->get("password_repeat")) {
//                $message = "Hasła nie pasują";
//                return [$message];
//            }

            $token = false;
            while (!$token) {
                $testToken = StringHelper::uniqueCode(32, true);
                if (!$userR->findOneByConfirmationToken($testToken)) {
                    $token = $testToken;
                }
            }

            $apiKey = false;
            while (!$apiKey) {
                $testApiKey = StringHelper::uniqueCode(32, true);
                if (!$userR->findOneByApiKey($testApiKey)) {
                    $apiKey = $testApiKey;
                }
            }

            $salt = false;
            while (!$salt) {
                $testSalt = StringHelper::uniqueCode(6, true);
                if (!$userR->findOneBySalt($testSalt)) {
                    $salt = $testSalt;
                }
            }

            $user = new User();
            $user->setUsername($username);
            $user->setEmail($email);
            $user->setPassword($passwordEncoder->encodePassword($user, $pass));
            $user->setSalt($salt);
            $user->setApiKey($apiKey);
            $user->setEnabled(false);
            $user->setPasswordRequestedAt(new \DateTime());
            $user->setRoles(null);
            $user->setConfirmationToken($token);
            $user->setFirstName(null);
            $user->setUsername($username);
            $user->setFirstName(null);
            $user->setLastName(null);
            $user->setUserType($userTypeR->find(1));
            $id = $dbS->saveRecord($user);

            if ($id) {
                //można by maila wyslać
            }

            $message = "Sprawdź swoją skrzynkę " . $email . " i aktywuj konto... Ale tego nie rób bo nie ma wysyłki";
            return ['message' => $message, 'status' => 'success', $data = $user];
        }

        return null;
    }
}