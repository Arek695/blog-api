<?php


namespace App\Controller;


use App\Service\DbService;

class DefaultController
{
    /**
     * Prawidłowy format danych dla jQuery POST i GET
     * Wszystkie funkcje zbierania danych mają zwracać:
     * albo jeden string, jeden number lub jeden boolean,
     * albo format:
     * [
     *    'costam1'=>['k1'=>'v1', 'k2'=>'v2', ...],
     *    'costam2'=>['k1'=>'v1', 'k2'=>'v2', ...],
     *    'costam3'=>'jakies_dane',
     *    'message'=>'komunikat', // jeśli nie $view, opcjonalnie, string
     *    'alert'=>'alert', // jeśli nie $view, opcjonalnie, string
     *    'error'=>'error' // jeśli nie $view, opcjonalnie, string
     * ]
     * podając $view można zwracać fragment widoku dla arraya lub obiektu
     *
     * @param mixed $data
     * @param null $view
     * @return array
     */
    public function setResponseData($data, $view = null): array
    {
        $result = [];
        if (!$data) {
            $result = $this->defaultS->emptyContentAndMessage();
        } elseif (is_string($data) || is_numeric($data) || is_bool($data)) {
            $result['content'] = $data;
        } else {
            // poniższa linia odda wszystkie dane wraz z komunikatami, co może się przydać
            $result = is_array($data) ? $data : (array) $data;

            if (!count($result)) {
                $result = $this->defaultS->emptyContentAndMessage();
            } elseif ($view) {
                // w tym przypadku message, alert i error musi być obsłużony
                // przez fragment widoku, miejscowo przez ten widok
                $result['content'] = $this->renderView($view, $result);
            }
        }

        return $result;
    }
}