<?php
namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping\UniqueConstraint;

/**
 * User
 *
 * @ORM\Table(name="users", uniqueConstraints={@ORM\UniqueConstraint(name="api_key", columns={"api_key"}), @ORM\UniqueConstraint(name="username", columns={"username"}), @ORM\UniqueConstraint(name="slug", columns={"slug"}), @ORM\UniqueConstraint(name="email", columns={"email"}), @ORM\UniqueConstraint(name="confirmation_token", columns={"confirmation_token"})})
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"username"},message="Ten login już jest zajęty!")
 * @UniqueEntity(fields={"email"},message="Ten e-mail został już zarejestrowany!")
 * @method string getUserIdentifier()
 */
class User implements UserInterface {

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;


    /**
     * @var string
     *
     * @ORM\Column(name="username", type="string", length=180, unique=true, nullable=false)
     */
    private $username;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=180, unique=true, nullable=false)
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string", length=255, nullable=true)
     */
    private $password;

    /**
     * @var string|null
     *
     * @ORM\Column(name="salt", type="string", length=255, nullable=true, options={"default"="NULL"})
     */
    private $salt;

    /**
     * @var string|null
     *
     * @ORM\Column(name="api_key", type="string", length=32, unique=true, nullable=true, options={"default"="NULL"})
     */
    private $apiKey = null;

    /**
     * @var bool
     *
     * @ORM\Column(name="enabled", type="boolean", nullable=false, options={"default"="0"})
     */
    private $enabled = '0';


    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="password_requested_at", type="datetime", nullable=true, options={"default"="CURRENT_TIMESTAMP"})
     */
    private $passwordRequestedAt = 'CURRENT_TIMESTAMP';

    /**
     * @var string
     *
     * @ORM\Column(name="roles", type="text", length=0, nullable=true, options={"default"=NULL,"comment"="DC2Type:array"})
     */
    private $roles = null;

    /**
     * @var string
     *
     * @ORM\Column(name="confirmation_token", type="string", length=180, unique=true, nullable=true)
     */
    private $confirmationToken;


    /**
     * @var string
     *
     * @ORM\Column(name="first_name", type="string", length=50, nullable=false)
     */
    private $firstName;

    /**
     * @var string
     *
     * @ORM\Column(name="last_name", type="string", length=50, nullable=false)
     */
    private $lastName;

    /**
     * @var \UserType|null
     *
     * @ORM\ManyToOne(targetEntity="UserType")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_type_id", referencedColumnName="id")
     * })
     */
    private $userType = null;



    public function getId(): ?int {
        return $this->id;
    }


    public function getUsername(): ?string {
        return $this->username;
    }

    public function setUsername(string $username): self {
        $this->username = $username;

        return $this;
    }

    public function getEmail(): ?string {
        return $this->email;
    }

    public function setEmail(string $email): self {
        $this->email = $email;

        return $this;
    }

    public function getPassword(): ?string {
        return $this->password;
    }

    public function setPassword(string $password): self {
        $this->password = $password;

        return $this;
    }

    public function getSalt(): ?string {
        return $this->salt;
    }

    public function setSalt(?string $salt): self {
        $this->salt = $salt;

        return $this;
    }

    public function getApiKey(): ?string {
        return $this->apiKey;
    }

    public function setApiKey(?string $apiKey): self {
        $this->apiKey = $apiKey;

        return $this;
    }

    public function getEnabled(): ?bool {
        return $this->enabled;
    }

    public function setEnabled(bool $enabled): self {
        $this->enabled = $enabled;

        return $this;
    }

    public function getPasswordRequestedAt(): ?\DateTimeInterface {
        return $this->passwordRequestedAt;
    }

    public function setPasswordRequestedAt(?\DateTimeInterface $passwordRequestedAt): self {
        $this->passwordRequestedAt = $passwordRequestedAt;

        return $this;
    }

    public function getRoles(): ?array {
        $roles = $this->roles;
        if ($roles == null) {
            $r = [];
        } else {
            $r = explode(',', $roles);
        }
        return array_unique($r);
    }

    /*
    * dane np. ['ROLE_ADMIN','ROLE_DEFAULT']
    */

    public function setRoles(?array $roles): self {
        if (is_null($roles)) {
            $this->roles = null;
        } elseif (is_array($roles)) {
            $this->roles = strtoupper(implode(',', $roles));
        }

        return $this;
    }

    public function addRole(string $role): self {
        $r = $this->getRoles();
        $k = false;
        if (!$r) {
            $r = [];
        } else {
            $k = array_search(strtoupper($role), $r);
        }
        if ($k === false) {
            $r[] = strtoupper($roles);
            return $this->setRoles($r);
        }

        return false;
    }

    public function removeRole(string $role): self {
        $r = $this->getRoles();
        $k = array_search(strtoupper($role), $r);
        if ($k !== false) {
            unset($r[$k]);
            return $this->setRoles($r);
        }

        return false;
    }

    public function getConfirmationToken(): ?string {
        return $this->confirmationToken;
    }

    public function setConfirmationToken(?string $confirmationToken): self {
        $this->confirmationToken = $confirmationToken;
        return $this;
    }

    public function eraseCredentials() {

    }


    public function getFirstName(): ?string {
        return $this->firstName;
    }

    public function setFirstName(?string $firstName): self {
        $this->firstName = $firstName;
        return $this;
    }

    public function getLastName(): ?string {
        return $this->lastName;
    }

    public function setLastName(?string $lastName): self {
        $this->lastName = $lastName;
        return $this;
    }

    public function getUserType(): ?UserType {
        return $this->userType;
    }

    public function setUserType(?UserType $userType): self {
        $this->userType = $userType;
        return $this;
    }


    public function __call(string $name, array $arguments)
    {
        // TODO: Implement @method string getUserIdentifier()
        // TODO: Do zbadania, coś nowego
    }
}
