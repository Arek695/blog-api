<?php


namespace App\Entity;

use App\Entity\User;
use Doctrine\ORM\Mapping as ORM;
/**
 * Post
 *
 * @ORM\Table(name="posts")
 * @ORM\Entity(repositoryClass="App\Repository\PostsRepository")
 */
class Post
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer", nullable=false, options={"unsigned"=true})
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255, nullable=true)
     */
    private $title = null;

    /**
     * @var string
     *
     * @ORM\Column(name="content", type="string", length=255, nullable=true)
     */
    private $content = null;


    /**
     * @var string
     *
     * @ORM\Column(name="short_content", type="string", length=255, nullable=true)
     */
    private $short_content = null;

    /**
     * @var \User
     *
     * @ORM\ManyToOne(targetEntity="User", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * })
     */
    private $user=null;




    public function getId(): ?int {
        return $this->id;
    }

    public function getContent() : String{
        return $this->content;
    }

    public function setContent(string $content){
        $this->content = $content;
        return $this;
    }

    public function getTitle() : String{
        return $this->title;
    }

    public function setTitle(string $title){
        $this->title = $title;
        return $this;
    }

    public function getShortContent() : String{
        return $this->short_content;
    }

    public function setShortContent(string $short_content){
        $this->short_content = $short_content;
        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

}