<?php


namespace App\Service;


use App\Entity\Post;
use App\Helper\DataHelper;
use App\Helper\StringHelper;
use App\Repository\CommentsRepository;
use App\Repository\PostsRepository;
use App\Repository\UserRepository;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class PostsService extends DefaultService
{
    private PostsRepository $postsR;
    private CommentsService $commentsS;
    private CommentsRepository $commentsR;

    public function __construct(CommentsRepository $commentsR, DbService $dbS, UserRepository $userR, CommentsService $commentsS, PostsRepository $postsR, ParameterBagInterface $parameterBag, SessionInterface $session, Security $security)
    {
        $this->postsR = $postsR;
        $this->commentsS = $commentsS;
        $this->commentsR = $commentsR;
        $this->userR = $userR;
        $this->dbS = $dbS;
    }


    public function postsContent(?int $page = 1, ?int $limit = 20){
        $pageNr = $page ? $page : 1;
        $limitNr = $limit ? $limit : 20;
        try{
            $data = DataHelper::paginate($this->postsR->createQueryBuilder('p'),$page, $limit);
            $message = "Success";
        }catch(\Exception $e){
            $data = $e->getMessage();
            $message = "Rzucono watek";
            return ['message' => $message, 'status' => 'error', 'data' => $data];

        }
        return ['message' => $message, 'status' => 'success', 'data' => $data];
    }


    public function addPost(array $data = null){
        if(!$data){
            $message = "Błąd! Dane nie zostały przesłane!";;
            return ['message' => $message, 'status'=>'error', 'data' => $data];
        }
        try{
            $post = new Post();
            $post->setContent(StringHelper::filterStr($data['content']));
            $post->setShortContent(StringHelper::filterStr($data['short_content']));
            $post->setTitle(StringHelper::filterStr($data['title']));
            $post->setUser($this->userR->find(StringHelper::filterInt($data['user_id'])));
            if($this->dbS->saveRecord($post)){
                $data = $post;
                $message = "Post dodany poprawnie";
            }else{
                $message = "Błąd przy dodawaniu wpisu";
                return ['message' => $message, 'status' => 'error', 'data' => $data];
            }
        }catch (\Exception $exception){
            $message = "Wrzucony wątek";
            $data = $exception->getMessage();
            return ['message' => $message, 'status' => 'error', 'data' => $data];

        }
        return ['message' => $message, 'status' => 'success', 'data' => $data];
    }

    public function removePost(int $post_id = null){
        if(!$post_id){
           $message = "Błąd! Dane nie zostały przesłane!";;
            return ['message' => $message, 'status' => 'error', 'data' => null];
        }

        try{
            $post = $this->postsR->find(StringHelper::filterInt($post_id));
            if($this->dbS->deleteRecord($post)) {
                $message = "Wpis został usunięty!";
                $data = $post;
            }else{
                $message = "Błąd! Element nie został usunięty!";
                return ['message' => $message, 'status' => 'error', 'data' => null];
            }
        }catch (\Exception $exception){
            $message = "Wrzucony wyjątek";
            $data = $exception->getMessage();
            return ['message' => $message, 'status' => 'error', 'data' => $data];
        }
        return ['message' => $message, 'status' => 'success', 'data' => $data];

    }

    public function editPost(array $data = null){
        if(!$data){
            $message = "Błąd! Dane nie zostały przesłane!";;
            return ['message' => $message, 'status' => 'error', 'data' => $data];
        }
        try{
            $post = $this->postsR->find(StringHelper::filterInt($data['id']));
            in_array('title', $data) ? $post->setTitle(StringHelper::filterStr($data['title'])): '';
            in_array('short_content', $data) ? $post->setShortContent(StringHelper::filterStr($data['short_content'])): '';
            in_array('content', $data) ? $post->setContent(StringHelper::filterStr($data['content'])): '';
            in_array('user_id', $data) ? $post->setUser($this->userR->find(StringHelper::filterInt($data['user_id']))): '';
            if($this->dbS->saveRecord($post)){
                $data = $post;
                $message = "Wpis edytowany poprawnie";
            }else{
                $message = "Błąd przy edycji wpisu";
                return ['message' => $message, 'status' => 'error', 'data' => $data];

            }
        }catch (\Exception $exception){
            $message = "Wrzucony wątek";
            $data = $exception->getMessage();
            return ['message' => $message, 'status' => 'error', 'data' => $data];
        }

        return ['message' => $message, 'status' => 'success', 'data' => $data];
    }

    public function getPost(int $post_id){
        try{
            $message = "success";
            $data = $this->postsR->findOneBy(['id' => $post_id]);
            $data['comments'] = $this->commentsR->findBy(['post' => $data->getId()]);
            return ['message' => $message, 'status' => 'success', 'data' => $data];

        }catch (\Exception $exception){
            $message = "Wrzucony wątek";
            $data = $exception->getMessage();
            return ['message' => $message, 'status' => 'error', 'data' => $data];
        }
    }

}