<?php

namespace App\Service;

use App\Repository\UserRepository;
use App\Service\DbService;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class DefaultService {

    protected DbService $dbS;

    public function __construct(
        ParameterBagInterface $parameterBag,
        SessionInterface $session,
        Security $security,
        DbService $dbS,
        UserRepository $userR,
    ){
        $this->parameterBag = $parameterBag;
        $this->session = $session;
        $this->security = $security;
        $this->dbS = $dbS;
        $this->userR = $userR;
    }

    public function getRootDir(){
        return $this->parameterBag->get('kernel.project_dir') . '/';
    }
    public function getPublicDir() {
        return $this->getRootDir() . 'public/';
    }

    public function getParam(string $param) {
        $parameter = null;
        if($this->parameterBag->has($param)){
            $parameter = $this->parameterBag->get($param);
        }
        return $parameter;
    }

    protected function getUser(){
        return $this->security->getUser();
    }

    public function getSecurity() {
        return $this->security;
    }

    public function emptyContentAndMessage(){
        return [
            'message' => "",
            'status' => 'warning'
        ];
    }

}