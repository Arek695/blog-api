<?php


namespace App\Service;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
* Szybsza orientacja z bazą
*/
class DbService  {

	private EntityManagerInterface $em;
	
    public function __construct(
		EntityManagerInterface $em
	){
        $this->em = $em;
    }
	
	public function deleteRecord($dbRecord): bool
    {
		try {
			$this->em->persist($dbRecord);
			$this->em->remove($dbRecord);
			$this->em->flush();
		} catch (\Exception $e) {
			$log = new LogService('error.log');
			$log->error($e);
			return false;
		}
		
		return true;
	}
	
	public function saveRecord($dbRecord): bool
    {
		try {
			$this->em->persist($dbRecord);
			$this->em->flush();
		} catch (\Exception $e) {
			$log = new LogService('error.log');
			$log->error($e);
			return false;
		}
		
		return $dbRecord->getId();
	}

}