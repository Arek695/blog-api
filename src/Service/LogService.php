<?php

namespace App\Service;

use App\Helper\StringHelper;


class LogService {

    private $handle;

    /**
     * Constructor
     *
     * @param	string	$filename
     */
    public function __construct($filename) {
        $this->handle = fopen(DIR_LOGS . $filename, 'a');
    }

    /**
     * Zapis logu z błędami
     *
     * @param string $function
     * @param array $params
     * @param object $e
     */
    public function error(object $e) {
        $data  = $this->callBackTraceData();
        $data['error'] = $e->getMessage();
        if (DEV) {
            $file = $e->getFile() . ' #' . $e->getLine();
            $data['error_file'] = $file ? $file : '';
            $trace = $e->getTraceAsString();
            $data['error_trace'] = $trace ? $trace : '';
        }
        $this->write($data);
    }

    /**
     * Manualny zapis logu
     *
     * @param	string	$message
     */
    public function write($message) {
        if (is_object($message)) {
            $message = (array) $message;
        }

        $str = '';
        if (is_array($message)) {
            foreach ($message as $key => $val) {
                $v = is_object($val) || is_array($val) ?
                    trim(strip_tags(print_r($val, 1))) :
                    trim(strip_tags((string) $val));
                $str .= $key == 'error_trace' ? "\n$v" : "\n● $key :: $v";
            }
        } else {
            $str = !empty(trim((string) $message)) ? ' || ' . trim((string) $message) : '';
        }
        $time = date('Y-m-d H:i:s');
        fwrite($this->handle, "●< $time $str \n>●\n");
    }

    private function callBackTraceData(): array{
        $call = [];
        $exec = [];
        $t = debug_backtrace();
        $call = $this->getCall('call', $t, 3);
        $exec = $this->getCall('exec', $t, 2);

        return array_merge($call, $exec);
    }

    private function getCall($type, $t, $i): array{
        $c = [];
        if (isset($t[$i])) {
            if (array_key_exists('file', $t[$i])) {
                $c[$type . '_file'] = $t[$i]['file'] . (array_key_exists('line', $t[$i]) ? ' #' . $t[$i]['line'] : '');
            }
            if (array_key_exists('class', $t[$i])) {
                $c[$type . '_class'] = $t[$i]['class'];
            }
            if (array_key_exists('function', $t[$i])) {
                $c[$type . '_function'] = $t[$i]['function'];
            }
            if (array_key_exists('args', $t[$i])) {
                $c[$type . '_args'] = $t[$i]['args'];
            }
        }
        return $c;
    }

    /**
     *
     *
     */
    public function __destruct() {
        fclose($this->handle);
    }

}
