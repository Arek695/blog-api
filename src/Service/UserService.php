<?php


namespace App\Service;

use App\Entity\User;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use App\Helper\StringHelper;
use App\Repository\UserRepository;
use App\Repository\UserTypeRepository;

/**
 * HomeNexessitousService class
 */
class UserService extends DefaultService {


    public function __construct(
        ParameterBagInterface $parameterBag,
        SessionInterface $session,
        Security $security,
        DefaultService $defaultS,
        TranslatorInterface $translator,
        UserRepository $userR,
        UserTypeRepository $userTypeR,
        DbService $dbS
    ) {
        $this->dbS = $dbS;
        $this->defaultS = $defaultS;
        $this->userR = $userR;
        $this->userTypeR = $userTypeR;

    }


    public function delete($userId) {
        $user = $this->userR->find($userId);
        $id = false;
        if ($user) {
            $name = $user->getUsername();
            $id = $this->dbS->deleteRecord($user);
        }
        return $id ? ['status' => 'success', 'message' => "Usunięto $name", 'content' => 'remove'] :
            ['status' => 'danger', 'message' => "Błąd usuwania!", 'content' => false];
    }

    public function userData($user_id) {
        return $this->userR->find($user_id);
    }

    // zła nazwa - tylko admin uczelni!
    public function createUser($query, $academySlug, $academyId, $partner = 0) {
        $email = trim(strtolower($query['admin_email']));
        if (!EmailHelper::validateEmail($email)) {
            return [
                'status' => 'danger',
                'message' => $this->t('message.email.bad', ['%email%' => $email], 'messages', $this->locale)
            ];
        }


        $plainPassword = $this->randomPassword();

        $token = false;
        while (!$token) {
            $testToken = StringHelper::uniqueCode(32, true);
            if (!$this->userR->findOneByConfirmationToken($testToken)) {
                $token = $testToken;
            }
        }
        $slug = false;
        $x = '';
        while (!$slug) {
            $testSlug = StringHelper::clearSlug($academySlug) . $x;
            if (!$this->userR->findOneBySlug($testSlug)) {
                $slug = $testSlug;
            } else {
                $x = empty($x) ? 1 : $x + 1;
            }
        }
        $apiKey = false;
        while (!$apiKey) {
            $testApiKey = StringHelper::uniqueCode(32, true);
            if (!$this->userR->findOneByApiKey($testApiKey)) {
                $apiKey = $testApiKey;
            }
        }

        $salt = false;
        while (!$salt) {
            $testSalt = StringHelper::uniqueCode(6, true);
            if (!$this->userR->findOneBySalt($testSalt)) {
                $salt = $testSalt;
            }
        }

        if ($this->userR->findOneByEmail($email)) {
            $user = $this->userR->findOneByEmail($email);
            $user->setUserType($this->userTypeR->find($partner ? 4 : 3));
            $user->setEnabled(true);
            $id = $this->dbS->saveRecord($user);
            $message = 'Uzytkownik ' . $user->getUsername() . ' już istniał i przypisano mu rolę administratora.';
        } else {
            $user = new User();
            $user->setEmail($email);
            $user->setPassword($this->passwordEncoder->encodePassword($user, $plainPassword));
            $user->setSalt($salt);
            $user->setApiKey($apiKey);
            $user->setEnabled(true);
            $user->setPasswordRequestedAt(new \DateTime());
            $user->setRoles(['ROLE_ACADEMY']);
            $user->setConfirmationToken($token);
            $user->setUsername($slug);
            $user->setFirstName(null);
            $user->setLastName(null);
            $id = $this->dbS->saveRecord($user);
            $message = $id ? "Nowy uzytkonik $slug został dodany i przypisano mu rolę administratora." : "Bład dodawania uzytkownika $slug.";
        }

        return [
            'status' => 'danger',
            'message' => "Przypisanie zakończyło się błędem!"
        ];
    }

}
