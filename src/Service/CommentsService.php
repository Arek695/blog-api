<?php


namespace App\Service;


use App\Helper\StringHelper;
use App\Repository\CommentsRepository;
use App\Repository\PostsRepository;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Symfony\Component\Security\Core\Security;

class CommentsService extends DefaultService
{
    #[Pure] public function __construct(PostsRepository $postsR, CommentsRepository $commentsR, ParameterBagInterface $parameterBag, SessionInterface $session, Security $security)
    {
        $this->commentsR = $commentsR;
        $this->postsR = $postsR;
    }

    public function getCommentsFor(?int $post_id = null){
        if(!$post_id){
            $message = "Błąd! Dane nie zostały przesłane!";;
            return ['message' => $message, 'status' => 'warning', 'data' => null];
        }
        try{
            $data = $this->commentsR->findBy(['post' => $this->postsR->find($post_id)]);
            return $data;
            return ['message' => "success", 'status' => 'success', 'data' => $data];
        }catch (\Exception $exception){
            $data = "Wrzucony wątek: " . $exception->getMessage();
            return ['message' => $exception->getMessage(), 'status' => 'error', 'data' => $data];

        }

    }

    public function addCommentFor(?int $post_id = null, array $data = null,){
        if(!$data){
            return;
        }
    }

    public function removeCommentFor(?int $post_id = null){
        return true;
    }

}