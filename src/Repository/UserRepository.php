<?php
namespace App\Repository;

use App\Entity\User;
use App\Service\UserService;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Helper\DataHelper;

/**
 * @method User|null find($id, $lockMode = null, $lockVersion = null)
 * @method User|null findOneBy(array $criteria, array $orderBy = null)
 * @method User[]    findAll()
 * @method User[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserRepository extends ServiceEntityRepository {

    public function __construct(ManagerRegistry $registry) {
        parent::__construct($registry, User::class);
    }

    /*
    Poniższe działa tylko, jeśli w config/packages/security.yaml będzie:
    providers:
    users:
    entity:
    class: App\Entity\User
    */
    public function loadUserByUsername($usernameOrEmail) {
        return $this->createQuery('SELECT u FROM App\Entity\User u WHERE u.username = :query OR u.email = :query')
            ->setParameter('query', $usernameOrEmail)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function usersData($params, $pageNr=1, $limit=50): array
    {
//		dd($params);
        $q = $this->createQueryBuilder('u')
            ->leftJoin('u.userType', 'ut');

        if(array_key_exists('phrase', $params) && $params['phrase']){
            $q->andWhere('(u.username LIKE :phrase OR u.email LIKE :phrase)')
                ->setParameter('phrase', '%'.$params['phrase'].'%');
        }
        if(array_key_exists('role', $params)){
            $q->andWhere('u.roles LIKE :role')->setParameter('role', '%'.$params['role'].'%');
        }
        if(array_key_exists('enabled', $params)){
            $q->andWhere('(u.enabled = :enabled)')->setParameter('enabled', $params['enabled']);
        }

        $q	->addOrderBy('u.passwordRequestedAt', 'DESC')
            ->addOrderBy('u.username');
//		dd($q->getQuery());
        return DataHelper::paginate($q, $pageNr, $limit);
    }

}
